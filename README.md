# Weldments Custom Library

## Instructions:
- Clone the repository in a reasonable place.
- Tell solidworks to look for weldments in the weldments-custom-library folder, wherever you cloned it:
- Open Solidworks
- Go to options (gear on top toolbar)
- Select File Locations
- Select Show Folders for: Weldment Profiles (near the bottom)
- Press Add
- Select the weldments-custom-library folder
- Give it permission
- Done



Now in weldments structural member, you can select wood or rectangular/square sections in the dropdown.

## Weldments for ansi inch box tube, lumber, etc.
From here: https://www.3dcontentcentral.com/features/112/files/secure/download-feature.aspx?id=792689#0

## Weldments for lumber
From here: https://www.3dcontentcentral.com/search.aspx?arg=weldments&tagsnavigator=weldments&SortBy=match&PageSize=10&page=3
Credit to M Burke
